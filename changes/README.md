# Usage of Proclamation to maintain changelogs

<!--
SPDX-License-Identifier: CC0-1.0
SPDX-FileCopyrightText: 2020 Collabora, Ltd. and the Proclamation contributors
-->

This project uses the [Proclamation][] tool to maintain changelogs. Contributors
to this project do not need to use Proclamation, but they are asked to write a
fragment for the changelog describing their change. See below for more details.

- Directory to run Proclamation in: `changes/`
  - Config file: default name (`.proclamation.json`)
- Location of the per-changelog-section directories: `changes/`

[Proclamation]: https://gitlab.com/ryanpavlik/proclamation

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Quick Start Instructions for Contributors](#quick-start-instructions-for-contributors)
- [About Proclamation and Usage Instructions](#about-proclamation-and-usage-instructions)
  - [Fragments](#fragments)
  - [References](#references)
- [Sample Usage Workflow](#sample-usage-workflow)
  - [During Development](#during-development)
  - [Preparing for a Release](#preparing-for-a-release)

## Quick Start Instructions for Contributors

- Get a merge request number for your change: this might involve pushing it as a
  WIP.
- Create a file in the appropriate section's directory, named `mr.YOURNUMBER.md`
- In that file, briefly describe your change as you would like it describe in
  the changelog for the next release. A common format is to start with a
  component, subsection, or type of change (bug fix, feature), then a colon,
  then a sentence describing the user impact of the change.
- If your changes affect multiple sections, you can have a file in each section
  describing the section-specific changes.
- If you want more than one changelog fragment per section in a single change,
  you can name the additional fragments `mr.YOURNUMBER.2.md`,
  `mr.YOURNUMBER.3.md`, etc. The additional number appended at the end will not
  appear in the output, but will keep the files separate and will affect the
  sort order of the fragments.
- If your change resolves an issue or otherwise references some issue or
  merge request, you can add those references to the beginning of your
  changelog fragment. See the full instructions below regarding
  [References](#references).

## About Proclamation and Usage Instructions

The "Proclamation" tool assembles changelogs, which incorporate fragments of
changelog text added by the author of a change in a specific location and
format.

### Fragments

Each change should add a changelog fragment file, whose contents are
Markdown-formatted text describing the change briefly. Reference metadata will
be used to automatically add links to associated issues/merge requests/pull
requests, so no need to add these in your fragment text. The simplest changelog
fragment just contains one line of Markdown text describing the change:

```md
Here the author of a change has written some text about it.
```

### References

The changelog fragment system revolves around "references" - these are issue
reports or merge requests associated with a change. Each fragment must have at
least one of these, which forms the main part of the filename. If applicable,
additional can be added within the file - see below for details.

The format of references in general is:

```txt
<ref_type>.<number>
```

where

- `ref_type` is "issue" or "mr"
- `number` is the issue or MR number

Your changelog fragment filename is simply the "main" reference with the `.md`
extension added.

Notably, you can append an extra token, like `.2`, before the file extension to
easily handle multiple fragments for a single merge request, etc.

To specify additional references in a file, prefix the contents of the changelog
fragment with a block delimited above and below by `---`, with one reference on
each line. (This can be seen as a very minimal subset of "YAML Front Matter", if
you're familiar with that concept.) For example:

```md
---
- issue.35
- mr.93
---
Here the author of a change has written some text about it.
```

## Sample Usage Workflow

Note that the base `proclamation` script and all its subcommands have help,
accessible through `-h` or `--help`. The guidance in this section of the README
is intentionally minimal, to avoid contradicting the online help which remains
up-to-date implicitly. This is also only the simplest, minimal way to perform
these operations: if your project is more complex, there may already be more
features to support your needs in the command line help.

### During Development

As changes get submitted to your project, have each change author create a
changelog fragment file. Since these are all separate files, with names made
unique by your issue/repo tracker, there won't be merge conflicts no matter what
order they're merged in. (This is the central benefit of Proclamation, and its
inspiration, towncrier, over having each contributor edit CHANGES as part of
their submission.)

At any time you can run `proclamation draft` to preview the release portion that
would be added to your changelog if you released at that time.

### Preparing for a Release

When you're ready to perform a release, you'll want to run Proclamation to
update your changelog, then remove the fragments that you've incorporated into
the regular changelog. You can use a command like the following:

```sh
proclamation build YOUR_NEW_VERSION
```

to preview the full file on screen. When you're ready to actually perform the
update, run something like:

```sh
proclamation build YOUR_NEW_VERSION --delete-fragments --overwrite
```

to overwrite your changelog file with the updated one and delete the used
changelog fragments.

You're welcome to manually edit the new (or old!) changelog entries as desired:
as long as the `insert_point_pattern` (by default, `^## .*`) can still match,
Proclamation will not be confused.

Finally, make sure the deletion of the fragments and the update of the changelog
has been checked in to your version control system.
